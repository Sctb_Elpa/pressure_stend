#include <cstring>

#include <QtDebug>

#include <QDateTime>
#include <QFont>
#include <QGraphicsLayout>
#include <QHBoxLayout>
#include <QImage>
#include <QImageReader>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QVector>

#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QtCharts/QDateTimeAxis>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>

#include "chartwindow.h"

// qtcreator: 4.10.2-1
// qtcreator: 4.12.3-1 -- cmake works!

static constexpr auto animationperiod = 150;
static constexpr auto minimal_diff = 10.0;

static constexpr auto max_points = 500;

static const QString stylesheet{
    R"V0G0N(QPushButton { background-color: red }
    QPushButton:checked { background-color: green }
    QPushButton#enabled { qproperty-icon: url(:/res/switch_off.png) }
    QPushButton#pumping { qproperty-icon: url(:/res/gear.png) })V0G0N"};

struct ChartWindow::Context {
  Context() {
    enable_indicator->setObjectName("enabled");
    pumping_indicator->setObjectName("pumping");

    enable_indicator->setCheckable(true);
    pumping_indicator->setCheckable(true);
  }

  uint32_t update_interval_ms;

  QtCharts::QChart *chart = new QtCharts::QChart;

  QtCharts::QDateTimeAxis *axisX = new QtCharts::QDateTimeAxis;
  QtCharts::QValueAxis *axisY = new QtCharts::QValueAxis;

  QtCharts::QLineSeries *series = new QtCharts::QLineSeries;

  QtCharts::QChartView *chartView = new QtCharts::QChartView(chart);

  QHBoxLayout *hlayout = new QHBoxLayout;
  QVBoxLayout *vlayout = new QVBoxLayout;

  QPushButton *enable_indicator = new QPushButton{"Включено"};
  QPushButton *pumping_indicator = new QPushButton{"Перекачка"};

  QLabel *levelLabel = new QLabel;

  QWidget *central_widget = new QWidget;

  QList<std::pair<QImage, QPointF>> backgrounds;
  QList<QImage> srcs;
  QList<std::pair<QImage, QPointF>>::iterator bg_it;
  QList<QImage>::const_iterator src_it;

  QImage *background = nullptr;
  QImageReader src{":/res/image.gif"};

  QSize current_bg_size;

  QVector<QPointF> points;

  int AnimationTimerID = -1;
};

template <typename T, typename U, typename V>
static void configure_chart(T chart, U xaxis, V yaxis) {
  chart->legend()->hide();
  chart->addAxis(xaxis, Qt::AlignBottom);
  chart->addAxis(yaxis, Qt::AlignRight);

  xaxis->setFormat("mm:ss");

  chart->setTitle("Уровень в резервуаре");

  QFont fnt;
  fnt.setPixelSize(20);

  chart->setTitleFont(fnt);
}

template <typename T, typename U, typename V, typename W>
static void configure_series(T series, U chart, V axisX, W axisY) {
  series->setPen(QPen(QColor(43, 31, 16), 3));
  series->blockSignals(true);
  // series->setUseOpenGL(true);
  chart->addSeries(series);
  series->attachAxis(axisX);
  series->attachAxis(axisY);
}

template <typename T, typename U, typename V>
static void append_point(T &list, U timestamp, V value) {
  QPointF newpoint{static_cast<qreal>(timestamp.toMSecsSinceEpoch()), value};

  if (list.size() >= max_points) {
    std::memmove(list.data(), &list.data()[1],
                 sizeof(list.data()[0]) * (max_points - 1));
    list.data()[max_points - 1] = std::move(newpoint);
  } else {
    list.append(newpoint);
  }
}

static void loadImages(QImageReader &src, QList<QImage> &list) {
  while (src.canRead()) {
    QImage img;
    src.read(&img);
    list.append(img);
  }
}

void ChartWindow::redraw_background() {
  rescale_if_needed();
  ctx->chart->setPlotAreaBackgroundBrush(*ctx->background);
}

ChartWindow::ChartWindow(uint32_t update_interval_ms)
    : QMainWindow(), ctx{std::make_unique<Context>()} {
  /*
    auto status = ctx->chart->layout()->instantInvalidatePropagation();

    ctx->chart->layout()->setInstantInvalidatePropagation(false);
  */

  ctx->update_interval_ms = update_interval_ms;

  configure_chart(ctx->chart, ctx->axisX, ctx->axisY);

  configure_series(ctx->series, ctx->chart, ctx->axisX, ctx->axisY);

  ctx->hlayout->addWidget(ctx->enable_indicator);
  ctx->hlayout->addWidget(ctx->pumping_indicator);
  ctx->hlayout->addWidget(ctx->levelLabel);

  ctx->vlayout->addWidget(ctx->chartView);
  ctx->vlayout->addLayout(ctx->hlayout);

  ctx->central_widget->setLayout(ctx->vlayout);
  ctx->central_widget->setStyleSheet(stylesheet);

  // ctx->chartView->setRenderHint(QPainter::Antialiasing, true);

  setCentralWidget(ctx->central_widget);

  // startTimer(animationperiod);

  loadImages(ctx->src, ctx->srcs);
  ctx->src_it = ctx->srcs.cbegin();
  ctx->bg_it = ctx->backgrounds.begin();

  ctx->chart->setPlotAreaBackgroundVisible(true);
}

ChartWindow::~ChartWindow() = default;

void ChartWindow::rescaleX() {
  auto from = QDateTime::fromMSecsSinceEpoch(ctx->points.first().x());
  auto to = (ctx->points.count() < max_points)
                ? from.addMSecs(ctx->update_interval_ms * max_points)
                : QDateTime::fromMSecsSinceEpoch(ctx->points.last().x());
  ctx->axisX->setRange(from, to);
}

void ChartWindow::rescaleY() {
  std::vector<qreal> ys(ctx->points.size());

  auto its = ctx->points.cbegin();
  auto itd = ys.begin();
  for (; its < ctx->points.cend(); ++its, ++itd) {
    *itd = its->y();
  }

  std::sort(ys.begin(), ys.end());

  auto from = ys.front();
  auto to = ys.back();

  if (std::abs(from - to) < minimal_diff) {
    auto middle = (from + to) / 2;
    from = middle - minimal_diff / 2;
    to = middle + minimal_diff / 2;
  }

  ctx->axisY->setRange(from, to);
}

bool ChartWindow::isTimerRunning() const { return ctx->AnimationTimerID != -1; }

void ChartWindow::AddData(const Result &input) {
  ctx->enable_indicator->setChecked(input.in_enable);

  ctx->pumping_indicator->setChecked(input.state_pumping_simulation);
  if (input.state_pumping_simulation != isTimerRunning()) {
    if (input.state_pumping_simulation) {
      ctx->AnimationTimerID = startTimer(animationperiod);
    } else {
      killTimer(ctx->AnimationTimerID);
      ctx->AnimationTimerID = -1;
    }
  }

  append_point(ctx->points, input.timestamp, input.sens_Pressure);

  ctx->series->blockSignals(true);

  rescaleX();
  rescaleY();

  ctx->series->blockSignals(false);

  ctx->series->replace(ctx->points);
}

void ChartWindow::updateYrange(qreal min, qreal max) {
  ctx->axisY->setRange(min, max);
}

void ChartWindow::rescale_if_needed() {
  const auto topLeft = ctx->chart->plotArea().topLeft();
  auto bottomRight = ctx->chart->plotArea().bottomRight();
  const QSize plot_area_size(bottomRight.x() - topLeft.x(),
                             bottomRight.y() - topLeft.y());

  if ((ctx->bg_it == ctx->backgrounds.end()) ||
      (ctx->bg_it->first.size() != ctx->chart->size().toSize()) ||
      (ctx->bg_it->second != bottomRight)) {

    const auto target_size = plot_area_size / 3;

    auto scaled = ctx->src_it->scaled(target_size, Qt::IgnoreAspectRatio,
                                      Qt::SmoothTransformation);

    QImage dest(ctx->chart->size().toSize(), QImage::Format_ARGB32);

    dest.fill(Qt::transparent);
    QPainter painter(&dest);
    painter.setOpacity(0.35);

    painter.drawImage(
        topLeft.x() + plot_area_size.width() - target_size.width(),
        topLeft.y() + plot_area_size.height() - target_size.height(), scaled);

    if (ctx->bg_it == ctx->backgrounds.end()) {
      ctx->backgrounds.append(std::make_pair<QImage, QPointF>(
          std::move(dest), std::move(bottomRight)));
      ctx->bg_it = ctx->backgrounds.end();

      if (++ctx->src_it == ctx->srcs.cend()) {
        ctx->src_it = ctx->srcs.cbegin();
        ctx->bg_it = ctx->backgrounds.begin();
      }

      ctx->background = &ctx->backgrounds.last().first;

      return;
    } else {
      *ctx->bg_it = std::make_pair<QImage, QPointF>(std::move(dest),
                                                    std::move(bottomRight));
      ctx->background = &ctx->bg_it->first;
    }
  } else {
    ctx->background = &ctx->bg_it->first;
  }

  // next frame + wrap
  if (++ctx->bg_it == ctx->backgrounds.end()) {
    ctx->bg_it = ctx->backgrounds.begin();
    ctx->src_it = ctx->srcs.cbegin();
  } else {
    ++ctx->src_it;
  }
}

void ChartWindow::timerEvent(QTimerEvent *ev) {
  redraw_background();
  ev->accept();
}
