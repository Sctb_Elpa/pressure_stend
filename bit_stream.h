#ifndef BIT_STREAM_H
#define BIT_STREAM_H

// https://stackoverflow.com/a/48643036

// struct to hold the value:
template <typename T> struct bits_t { T t; }; // no constructor necessary
// functions to infer type, construct bits_t with a member initialization list
// use a reference to avoid copying. The non-const version lets us extract too
template <typename T> bits_t<T &> bits(T &t) { return bits_t<T &>{t}; }
template <typename T> bits_t<const T &> bits(const T &t) {
  return bits_t<const T &>{t};
}
// insertion operator to call ::write() on whatever type of stream
template <typename S, typename T> S &operator<<(S &s, bits_t<T> b) {
  return s.write((char *)&b.t, sizeof(T));
}
// extraction operator to call ::read(), require a non-const reference here
template <typename S, typename T> S &operator>>(S &s, bits_t<T &> b) {
  return s.read((char *)&b.t, sizeof(T));
}

#endif // BIT_STREAM_H
