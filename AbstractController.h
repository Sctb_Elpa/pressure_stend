#ifndef ABSTRACTCONTROLLER_H
#define ABSTRACTCONTROLLER_H

#include <QObject>

#include "Result.h"

#include "ICommand.h"

class AbstractController : public QObject {
  Q_OBJECT
public:
  explicit AbstractController(float LowerLimit, float UpperLimit,
                              QObject *parent = nullptr);

public slots:
  virtual void Input(const Result &input) = 0;

signals:
  void newCommand(const ICommand &cmd);

protected:
  float LowerLimit;
  float UpperLimit;

  void stop_if_not(const Result &input);
  void start_if_not(const Result &input);
};

#endif // ABSTRACTCONTROLLER_H
