#ifndef RESSIVER_H
#define RESSIVER_H

#include <QObject>

#include "Result.h"

class QAbstractSocket;

class Ressiver : public QObject {
  Q_OBJECT
public:
  Ressiver(QAbstractSocket &socket, QObject *parent = nullptr);

private slots:
  void dataRessived();

signals:
  void newDataRessived(const Result &res);

private:
  QAbstractSocket &socket;
};

#endif // RESSIVER_H
