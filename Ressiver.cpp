#include <tuple>

#include <google/protobuf/io/zero_copy_stream_impl_lite.h>

#include <QAbstractSocket>

#include "pressure_stend.pb.h"

#include "Ressiver.h"

using Response = ru::sktbelpa::pressure_stend::Response;

using namespace google::protobuf::io;

static bool check_magick(CodedInputStream &is) {
  char m;
  is.ReadRaw(&m, sizeof(m));

  return m == ru::sktbelpa::pressure_stend::INFO::MAGICK;
}

static uint64_t getMsgSize(CodedInputStream &is) {
  uint64_t res;
  is.ReadVarint64(&res);
  return res;
}

static Result toResult(uint64_t timesatmp,
                       const ru::sktbelpa::pressure_stend::Status &status) {
  return Result{QDateTime::fromMSecsSinceEpoch(timesatmp),
                status.sens_pressure(),
                status.sens_temperature(),
                status.in_enable(),
                status.in_force(),
                status.in_pump_simulator_at_start_positon(),
                status.state_pumping_simulation(),
                status.state_enable_pump()};
}

Ressiver::Ressiver(QAbstractSocket &socket, QObject *parent)
    : QObject(parent), socket{socket} {
  connect(&socket, &QAbstractSocket::readyRead, this, &Ressiver::dataRessived);
}

void Ressiver::dataRessived() {
  Response resp;

  auto data = socket.readAll();

  if (data.size() > 0) {
    ArrayInputStream arrayInput(data.data(), data.size());
    CodedInputStream codedInput(&arrayInput);

    if (check_magick(codedInput)) {
      auto msg_size = getMsgSize(codedInput);
      if (1 + 1 + msg_size == data.size()) {
        resp.ParseFromCodedStream(&codedInput);

        if (resp.global_status() == ru::sktbelpa::pressure_stend::STATUS::OK) {
          emit newDataRessived(toResult(resp.timestamp(), resp.status()));
          return;
        }

        qDebug() << "Error: "
                 << QString::fromStdString(
                        ru::sktbelpa::pressure_stend::STATUS_Name(
                            resp.global_status()));
      }
      qDebug() << "incorrect size: " << msg_size << " != " << data.size() - 2;
    }
    qDebug() << "Incorrect magick";
  }
  qDebug() << "Empty data ressived";
}
