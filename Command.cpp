#include "ICommand.h"

bool ICommand::has_enable_pumping_simulation() const { return false; }

bool ICommand::enable_pumping_simulation() const { return false; }

bool ICommand::has_enable_pump() const { return false; }

bool ICommand::enable_pump() const { return false; }
