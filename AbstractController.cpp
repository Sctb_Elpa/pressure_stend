#include "AbstractController.h"

AbstractController::AbstractController(float LowerLimit, float UpperLimit,
                                       QObject *parent)
    : QObject(parent), LowerLimit{LowerLimit}, UpperLimit{UpperLimit} {}

void AbstractController::stop_if_not(const Result &input) {
  if (input.state_enable_pump || input.state_pumping_simulation) {
    emit newCommand(StopCommand());
  }
}

void AbstractController::start_if_not(const Result &input) {
  if (!input.state_pumping_simulation) {
    emit newCommand(StartSymCommand());
  }
}
