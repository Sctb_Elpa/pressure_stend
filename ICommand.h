#ifndef ICOMMAND_H
#define ICOMMAND_H

struct ICommand {
  virtual ~ICommand() = default;

  virtual bool has_enable_pumping_simulation() const;
  virtual bool enable_pumping_simulation() const;
  virtual bool has_enable_pump() const;
  virtual bool enable_pump() const;
};

template <bool is_enable_pumping_simulation, bool en_pumping_simulation,
          bool is_enable_pump, bool en_pump>
struct Command : public ICommand {
  bool has_enable_pumping_simulation() const override {
    return is_enable_pumping_simulation;
  }
  bool enable_pumping_simulation() const override {
    return en_pumping_simulation;
  }
  bool has_enable_pump() const override { return is_enable_pump; }
  bool enable_pump() const override { return en_pump; }
};

using StopCommand = Command<true, false, true, false>;
using StartSymCommand = Command<true, true, false, false>;
using StartPumCommand = Command<false, false, true, true>;
using StopPumCommand = Command<false, false, true, false>;

#endif // ICOMMAND_H
