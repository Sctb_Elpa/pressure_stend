#ifndef SIMPLE_CONTROLLER_H
#define SIMPLE_CONTROLLER_H

#include "AbstractController.h"

class SimpleController : public AbstractController {
public:
  explicit SimpleController(float LowerLimit, float UpperLimit,
                            QObject *parent = nullptr);

  void Input(const Result &input) override;

private:
  enum class State { DISABLED, PUMPING, PUMP_FORCE, WAITING };

  State state;
};

#endif // SIMPLE_CONTROLLER_H
