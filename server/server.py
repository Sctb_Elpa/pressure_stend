#!/usr/bin/env python3

from proto.pressure_stend_pb2 import Request, Response, Status, INFO, STATUS
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from google.protobuf.internal.decoder import _DecodeVarint32
from google.protobuf.internal.encoder import _VarintBytes

import time
import struct
from Controller import Controller

udp_port = 9178

# если не работает, проверь, что protobuf действительно использует cpp бэкэнд, не python - он сломан!
# >>> from google.protobuf.internal import api_implementation
# >>> api_implementation.Type()
# 'cpp'
# из pip устанавливается без 'cpp' как минимум на "собаке"
# Скачай дистрибутив protobuf и читай README.md Весь компилятор собирать не нужно!


sym_pumping_pin = 20
pump_pin = 21

enable_pin = 13
force_pin = 19
sym_at_start_pos = 26

sensor_i2c_addr = 0x0f
sensor_i2c_bus = 1


def get_magick(msg):
    return msg[1:], msg[0]


def prepare_response(msg, src_id, status):
    msg.id = src_id
    msg.deviceID = INFO.Value('PRESSURE_STEND_ID')
    msg.protocolVersion = INFO.Value('PROTOCOL_VERSION')
    msg.Global_status = status
    msg.timestamp = int(time.time() * 1000)


class Server(DatagramProtocol):
    def __init__(self, controller):
        self.controller = controller

    def process_request(self, req):
        if req.HasField('control'):
            control = req.control
            if control.HasField('enable_pumping_simulation'):
                self.controller.pumping_simulation = control.enable_pumping_simulation
            if control.HasField('enable_pump'):
                self.controller.enable_pump = control.enable_pump

    def decode(self, msg):
        msg, magick = get_magick(msg)
        if magick != INFO.Value('MAGICK'):
            return None

        res = Request()
        try:
            msg_len, new_pos = _DecodeVarint32(msg, 0)
            if msg_len > 1500:
                return None

            res.ParseFromString(msg[new_pos:])
        except Exception as ex:
            return None

        return res

    def encode(self, msg):
        magick = bytes([INFO.Value('MAGICK')])
        size = _VarintBytes(msg.ByteSize())
        body = msg.SerializeToString()
        return magick + size + body

    def fill_controller_satus(self):
        status = Status()

        status.in_enable = self.controller.enabled
        status.in_force = self.controller.forced
        status.in_pump_simulator_at_start_positon = self.controller.pump_simulator_at_start_positon
        status.state_pumping_simulation = self.controller.pumping_simulation
        status.state_enable_pump = self.controller.enable_pump

        status.sens_Pressure = self.controller.pressure
        status.sens_Temperature = self.controller.temperature

        return status

    def datagramReceived(self, data, addr):
        msg = self.decode(data)
        if msg and (msg.deviceID == INFO.Value('PRESSURE_STEND_ID') or
                    msg.deviceID == INFO.Value('ID_DISCOVER')):
            self.process_request(msg)

            resp = Response()

            prepare_response(resp, msg.id, STATUS.Value('OK'))

            resp.status.CopyFrom(self.fill_controller_satus())

            resp_data = self.encode(resp)
            self.transport.write(resp_data, addr)
        else:
            self.transport.write(data, addr)


if __name__ == "__main__":
    reactor.listenUDP(udp_port, Server(Controller(enable_pin, force_pin, sym_at_start_pos,
                                                  sym_pumping_pin, pump_pin,
                                                  sensor_i2c_addr, sensor_i2c_bus)))
    reactor.run()
