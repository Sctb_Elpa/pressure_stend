#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import smbus
import struct
import sys
import time


def parceFloat(b):
    b = bytes(b)
    return struct.unpack('<f', b)[0]


def splitFloats(l):
    n = 4
    return (l[i:i+n] for i in range(0, len(l), n))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-F', '--Freq-Only', dest='freq_only', default=False, action='store_true',
                        help='Читать только частоты')
    parser.add_argument('-A', '--i2c-addr', dest='dev_addr', default=0x08, type=int,
                        help='Адрес устройства i2c')
    parser.add_argument('-B', '--i2c-bus', dest='bus', default=-1, type=int,
                        help='Номер шины i2c')
    parser.add_argument('-T', '--period', default=0.1, type=float,
                        help='Период опроса, с')
        
    args = parser.parse_args()

    i2c = smbus.SMBus(args.bus)

    start = 2 * 4 if args.freq_only else 0
    Len = 4 * 2 if args.freq_only else 4 * 4

    while True:
        res = i2c.read_i2c_block_data(args.dev_addr, start, Len)

        for r in map(parceFloat, splitFloats(res)):
            sys.stdout.write('{};'.format(r))

        sys.stdout.write('\n')
        time.sleep(args.period)
    

# чтобы при импорте не выполнялся код автоматом
if __name__ == '__main__':
    main()


