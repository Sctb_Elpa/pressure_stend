#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO


class GPIORpi:
    def __init__(self, pin, pull='up', invert=False):
        GPIO.setmode(GPIO.BCM)
        self.pin = pin
        self.invert = invert
        self.pull = pull
        self.dir = 'in'
        self.set_dir('in')
 
    def set_dir(self, direction):
        if direction == 'in':
            GPIO.setup(self.pin, GPIO.IN, pull_up_down=(GPIO.PUD_UP if self.pull == 'up' else GPIO.PUD_DOWN))
        elif direction == 'out':
            GPIO.setup(self.pin, GPIO.OUT)
        else:
            raise ValueError('Only in/out accepted')

        self.dir = direction

    def get_dir(self):
        return self.dir

    def get_val(self):
        return GPIO.input(self.pin) ^ self.invert

    def set_val(self, v):
        GPIO.output(self.pin, v ^ self.invert)

    def __str__(self):
        return 'GPIO{}:\tdir={}\tval={}'.format(
            self.pin,
            self.get_dir(),
            self.get_val())

    def __del__(self):
        self.set_dir('in')
