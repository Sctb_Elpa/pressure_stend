import platform
from builtins import int

if platform.machine() == 'armv7l':
    from GPIO_Rpi import GPIORpi as GPIO
    from sensor_reader import parceFloat, splitFloats
    import smbus
    symulation = False
else:
    import random
    symulation = True


def define_gpio(gpio_n, dir='in', invert=False):
    if symulation:
        return gpio_n

    pin = GPIO(gpio_n, invert=invert)
    pin.set_dir(dir)
    return pin


class Controller:
    def __init__(self, enable_pin, force_pin, start_pos_pin, pump_sym_pin, pump_pin, sensor_addr, sensor_i2c_bus):
        self.__P = 0.0
        self.__T = 0.0
        self.sensor_addr = sensor_addr

        def sym_reader():
            return False

        def gpio_reader(gpio):
            def __read():
                return gpio.get_val()

            return __read

        def gpio_writer(gpio):
            def __write(v):
                gpio.set_val(v)

            return __write

        if symulation:
            self.pump_symulating = False
            self.pumping = False

            def __get_sym_state():
                return self.pump_symulating

            def __set_sym_state(v):
                self.pump_symulating = v

            def __get_pumping():
                return self.pumping

            def __set_pumping(v):
                self.pumping = v
        else:
            self.i2c = smbus.SMBus(sensor_i2c_bus)

        self.enabled_rdr = sym_reader if symulation else gpio_reader(define_gpio(enable_pin, invert=True))
        self.forced_rdr = sym_reader if symulation else gpio_reader(define_gpio(force_pin, invert=True))
        self.pump_simulator_at_start_positon_rdr = sym_reader if symulation else gpio_reader(define_gpio(start_pos_pin, invert=True))

        pump_sym_gpio = define_gpio(pump_sym_pin, 'out')
        pump_gpio = define_gpio(pump_pin, 'out')

        self.pumping_simulation_rdr = __get_sym_state if symulation else gpio_reader(pump_sym_gpio)
        self.enable_pump_rdr = __get_pumping if symulation else gpio_reader(pump_gpio)

        self.pumping_simulation_wrr = __set_sym_state if symulation else gpio_writer(pump_sym_gpio)
        self.enable_pump_wrr = __set_pumping if symulation else gpio_writer(pump_gpio)

    def update_output_P(self):
        if not symulation:
            try:
                res = self.i2c.read_i2c_block_data(self.sensor_addr, 0, 4)
                self.__P, = map(parceFloat, splitFloats(res))
            except OSError:
                print("Warning! no sensor response!")
        else:
            self.__P = random.random()

    def update_output_T(self):
        if not symulation:
            try:
                res = self.i2c.read_i2c_block_data(self.sensor_addr, 4, 4)
                self.__T, = map(parceFloat, splitFloats(res))
            except OSError:
                print("Warning! no sensor response!")
        else:
            self.__T = random.random()

    @property
    def pressure(self):
        self.update_output_P()
        return self.__P

    @property
    def temperature(self):
        self.update_output_T()
        return self.__T

    @property
    def enabled(self):
        return self.enabled_rdr()

    @property
    def forced(self):
        return self.forced_rdr()

    @property
    def pump_simulator_at_start_positon(self):
        return self.pump_simulator_at_start_positon_rdr()

    @property
    def pumping_simulation(self):
        return self.pumping_simulation_rdr()

    @pumping_simulation.setter
    def pumping_simulation(self, enable):
        self.pumping_simulation_wrr(enable)

    @property
    def enable_pump(self):
        return self.enable_pump_rdr()

    @enable_pump.setter
    def enable_pump(self, enable):
        self.enable_pump_wrr(enable)
