#ifndef CHARTWINDOW_H
#define CHARTWINDOW_H

#include <QMainWindow>
#include <memory>

#include "Result.h"

class ChartWindow : public QMainWindow {
  Q_OBJECT

public:
  ChartWindow(uint32_t update_interval_ms);
  ~ChartWindow();

public slots:
  void AddData(const Result &input);
  void updateYrange(qreal min, qreal max);

private:
  struct Context;
  std::unique_ptr<Context> ctx;

  void rescale_if_needed();
  void redraw_background();

  void rescaleX();
  void rescaleY();

  bool isTimerRunning() const;

protected:
  void timerEvent(QTimerEvent *ev) override;
};
#endif // CHARTWINDOW_H
