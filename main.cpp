#include <QApplication>
#include <QTimer>
#include <QUdpSocket>

#include <CLI/CLI.hpp>

#include "Ressiver.h"
#include "Writer.h"

#include "ControllerWithSymulation.h"
#include "SimpleController.h"

#include "chartwindow.h"

static constexpr auto update_interval_ms = 150;
static constexpr uint16_t port = 9178;

struct Arguments {
  std::string server = "localhost";
  float LowerLimit = 0.0;
  float UpeerLimit = 0.0;

  bool useSym = true;
};

void configureArgumentParcer(CLI::App &app, Arguments &options) {
  app.add_flag("--sym,!--no-sym", options.useSym, "Use pumping symulation")
      ->expected(0);

  app.add_option("-s,--server", options.server,
                 "Use server (default: localhost)", true)
      ->expected(1);

  app.add_option("LowerLimit", options.LowerLimit,
                 "Pressure lower Limit (pump ON)")
      ->required();
  app.add_option("UpperLimit", options.UpeerLimit,
                 "Pressure upper Limit (pump OFF)")
      ->required();
}

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);

  CLI::App argparse{"PCM encoder"};
  Arguments options;

  configureArgumentParcer(argparse, options);
  CLI11_PARSE(argparse, argc, argv)

  QUdpSocket socket;
  QTimer update_timer;
  ChartWindow w{update_interval_ms};

  std::unique_ptr<AbstractController> controller(
      options.useSym
          ? static_cast<AbstractController *>(new ControllerWithSymulation{
                options.LowerLimit, options.UpeerLimit})
          : new SimpleController{options.LowerLimit, options.UpeerLimit});

  Writer writer{socket};
  Ressiver ressiver{socket};

  socket.connectToHost(QString::fromStdString(options.server), port);
  update_timer.setInterval(update_interval_ms);

  QObject::connect(&update_timer, &QTimer::timeout, &writer,
                   static_cast<void (Writer::*)()>(&Writer::SendRequest));

  QObject::connect(&ressiver, &Ressiver::newDataRessived, &w,
                   &ChartWindow::AddData);

  QObject::connect(&ressiver, &Ressiver::newDataRessived, controller.get(),
                   &AbstractController::Input);
  QObject::connect(
      controller.get(), &AbstractController::newCommand, &writer,
      static_cast<void (Writer::*)(const ICommand &)>(&Writer::SendRequest));

  w.setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint);
  w.showMaximized();

  update_timer.start();

  return a.exec();
}
