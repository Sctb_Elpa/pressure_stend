#include <QTimer>

#include "SimpleController.h"

SimpleController::SimpleController(float LowerLimit, float UpperLimit,
                                   QObject *parent)
    : AbstractController(LowerLimit, UpperLimit, parent),
      state{State::DISABLED} {}

void SimpleController::Input(const Result &input) {
  // inputs:
  // in_enable, in_force, in_pump_simulator_at_start_positon
  // pressure

  // outputs:
  // state_pumping_simulation, state_enable_pump

  if (!input.in_enable) {
    stop_if_not(input);
    state = State::DISABLED;
    return;
  }

  if (input.sens_Pressure > UpperLimit) {
    stop_if_not(input);
    state = State::WAITING;
    return;
  }

  switch (state) {
  case State::DISABLED:
    if (input.in_enable) {
      state = State::WAITING;
    }
    break;
  case State::PUMPING:
    break;
  case State::PUMP_FORCE:
    if (!input.in_force) {
      stop_if_not(input);
      state = State::WAITING;
    }
    break;
  case State::WAITING:
    stop_if_not(input);

    if (input.in_force) {
      start_if_not(input);
      state = State::PUMP_FORCE;
      return;
    }

    if (input.sens_Pressure < LowerLimit) {
      start_if_not(input);
      state = State::PUMPING;
    }

    break;

  default:
    break;
  }
}
