#ifndef RESULT_H
#define RESULT_H

#include <QDateTime>
#include <sstream>

struct Result {
  // timestamp
  QDateTime timestamp;

  // Давление, измеренное датчиком
  float sens_Pressure;
  // Температура, измеренная датчиком
  float sens_Temperature;

  // Вход: Разрещение работы
  bool in_enable;
  // Вход: Ручное включение накачки
  bool in_force;
  // Вход: Качалка в нижнем положении
  bool in_pump_simulator_at_start_positon;

  // Выход: Статус работы симулятора качалки
  bool state_pumping_simulation;
  // Выход: Статус включения водяного насоса
  bool state_enable_pump;

  std::string toString() const {
    std::stringstream ss;

    ss << "Timestamp: " << timestamp.toString().toUtf8().data()
       << "Pressure: " << sens_Pressure << std::endl
       << "Temperature: " << sens_Temperature << std::endl
       << std::boolalpha << "Enabled: " << in_enable << std::endl
       << "Forced: " << in_force << std::endl
       << "Pump_sensor:" << in_pump_simulator_at_start_positon << std::endl
       << "pump_sym: " << state_pumping_simulation << std::endl
       << "Pumping: " << state_enable_pump << std::endl;

    return ss.str();
  }
};

#endif // RESULT_H
