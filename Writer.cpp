#include <vector>

#include <google/protobuf/io/zero_copy_stream_impl_lite.h>

#include "bit_stream.h"

#include "pressure_stend.pb.h"

#include "Writer.h"

using Request = ru::sktbelpa::pressure_stend::Request;
using Control = ru::sktbelpa::pressure_stend::Control;

static void FillMondatoryFields(Request &req) {
  using namespace ru::sktbelpa::pressure_stend;

  req.set_id(0);
  req.set_deviceid(INFO::PRESSURE_STEND_ID);
  req.set_protocolversion(INFO::PROTOCOL_VERSION);
}

static void WriteMessage(QAbstractSocket &sock, Request &req) {
  using namespace google::protobuf::io;

  static constexpr char magick = ru::sktbelpa::pressure_stend::INFO::MAGICK;

  auto len = req.ByteSizeLong();

  std::vector<char> buf(1 + 1 + len);

  ArrayOutputStream arrayOutput(buf.data(), buf.size());
  CodedOutputStream codedOutput(&arrayOutput);

  codedOutput.WriteRaw(&magick, sizeof(magick));
  codedOutput.WriteVarint64(len);

  req.SerializeToCodedStream(&codedOutput);

  sock.write(buf.data(), buf.size());
}

Writer::Writer(QAbstractSocket &socket, QObject *parent)
    : QObject(parent), socket{socket} {}

void Writer::SendRequest(const ICommand &cmd) {
  Request req;
  FillMondatoryFields(req);

  auto control = new Control;
  if (cmd.has_enable_pumping_simulation()) {
    control->set_enable_pumping_simulation(cmd.enable_pumping_simulation());
  }

  if (cmd.has_enable_pump()) {
    control->set_enable_pump(cmd.enable_pump());
  }

  req.set_allocated_control(control);

  WriteMessage(socket, req);
}

void Writer::SendRequest() {
  Request req;
  FillMondatoryFields(req);
  WriteMessage(socket, req);
}
