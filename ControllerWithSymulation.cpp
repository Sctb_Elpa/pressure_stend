#include <QTimer>

#include "ControllerWithSymulation.h"

ControllerWithSymulation::ControllerWithSymulation(float LowerLimit,
                                                   float UpperLimit,
                                                   float cycle_duration,
                                                   QObject *parent)
    : AbstractController(LowerLimit, UpperLimit, parent),
      state{State::DISABLED}, calibState{CalibrationStage::IDLE}, calib_start{},
      cycle_duration{cycle_duration}, calibrated{false} {}

void ControllerWithSymulation::Input(const Result &input) {
  // inputs:
  // in_enable, in_force, in_pump_simulator_at_start_positon
  // pressure

  // outputs:
  // state_pumping_simulation, state_enable_pump

  if (!input.in_enable) {
    stop_if_not(input);
    state = State::DISABLED;
    return;
  }

  if (input.sens_Pressure > UpperLimit) {
    stop_if_not(input);
    state = State::WAITING;
    return;
  }

  switch (state) {
  case State::DISABLED:
    state = State::CALIBRATION;
    break;
  case State::CALIBRATION:
    perform_caibration(input);
    break;
  case State::PUMPING:
    // nothing
    start_pump_if_ready(input);
    break;
  case State::PUMP_FORCE:
    if (!input.in_force) {
      stop_if_not(input);
      state = State::WAITING;
    } else {
      start_pump_if_ready(input);
    }
    break;
  case State::WAITING:
    stop_if_not(input);

    if (input.in_force) {
      start_if_not(input);
      state = State::PUMP_FORCE;
      return;
    }

    if (input.sens_Pressure < LowerLimit) {
      start_if_not(input);
      state = State::PUMPING;
    }

    break;
  }
}

ControllerWithSymulation::Stend_State
ControllerWithSymulation::detect_current_state(const Result &input) {
  if (!input.in_enable) {
    return Stend_State::DISABLED;
  } else if (input.sens_Pressure > UpperLimit) {
    return Stend_State::OVER_LEVEL;
  } else if (input.in_force) {
    return Stend_State::FORCED;
  } else if (input.state_pumping_simulation) {
    return input.state_enable_pump ? Stend_State::PUMPING
                                   : Stend_State::PUMPING_SYM;
  } else if (input.sens_Pressure < LowerLimit) {
    return Stend_State::UNDER_LEVEL;
  } else {
    return Stend_State::IDLE;
  }
}

void ControllerWithSymulation::perform_caibration(const Result &input) {
  if (input.in_pump_simulator_at_start_positon) {
    if (calibState == CalibrationStage::IDLE) {
      calibState = CalibrationStage::START;
      calib_start = QTime::currentTime();
    } else if (calibState == CalibrationStage::WAITING) {
      calibState = CalibrationStage::IDLE;
      cycle_time = calib_start.msecsTo(QTime::currentTime());
      stop_if_not(input);
      state = State::WAITING;

      return;
    }
  } else if (calibState == CalibrationStage::START) {
    calibState = CalibrationStage::WAITING;
  }

  if (!input.state_pumping_simulation) {
    emit newCommand(StartSymCommand());
  }
}

void ControllerWithSymulation::start_pump_if_ready(const Result &input) {
  if (input.in_pump_simulator_at_start_positon &&
      input.state_pumping_simulation) {
    auto timer = new QTimer();

    timer->setSingleShot(true);
    connect(timer, &QTimer::timeout, [timer, this]() {
      emit newCommand(StopPumCommand());
      timer->deleteLater();
    });

    // start pump
    emit newCommand(StartPumCommand());
    timer->start(qRound(cycle_time * cycle_duration));
  }
  if (!input.state_pumping_simulation) {
    emit newCommand(StartSymCommand());
  }
}
