#ifndef CONTROLLER_WITH_SYMULATION_H
#define CONTROLLER_WITH_SYMULATION_H

#include "AbstractController.h"

class ControllerWithSymulation : public AbstractController {
public:
  explicit ControllerWithSymulation(float LowerLimit, float UpperLimit,
                                    float cycle_duration = 0.5,
                                    QObject *parent = nullptr);

  void Input(const Result &input) override;

private:
  enum class Stend_State {
    IDLE,     // просто ожидание, все выключено
    DISABLED, // Тумблер разрешения работы выключен
    FORCED, // Нажата кнопка Force, работа разрешена, и нет перелива
    UNDER_LEVEL, // Готов начать качать, но пока все ыключено
    OVER_LEVEL, // Заблокировано. Перелив
    PUMPING, // Есть разрешение, Насос качает, перелива нет.
    PUMPING_SYM // Есть разрешение, Насос не качает, только качалка
  };

  enum class State { DISABLED, CALIBRATION, PUMPING, PUMP_FORCE, WAITING };

  enum class CalibrationStage { IDLE, START, WAITING };

  State state;
  CalibrationStage calibState;

  QTime calib_start;
  int cycle_time;
  float cycle_duration;

  bool calibrated;

  Stend_State detect_current_state(const Result &input);

  void perform_caibration(const Result &input);
  void start_pump_if_ready(const Result &input);
};

#endif // CONTROLLER_WITH_SYMULATION_H
