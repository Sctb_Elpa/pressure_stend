#ifndef WRITER_H
#define WRITER_H

#include <QAbstractSocket>
#include <QObject>

#include "ICommand.h"

class Writer : public QObject {
  Q_OBJECT
public:
  explicit Writer(QAbstractSocket &socket, QObject *parent = nullptr);

public slots:
  void SendRequest(const ICommand &cmd);
  void SendRequest();

private:
  QAbstractSocket &socket;
};

#endif // WRITER_H
